/**
 * This is a demo class that only has a function for returning a fixed text
 */
public class PrintText {
    /**
     * @return A fixed text for demonstrating that the artifact works
     */
    public String printText(){
        return "This is a demo artifact for showing the necessary configuration for uploading artifacts to Maven Central";
    }
}
