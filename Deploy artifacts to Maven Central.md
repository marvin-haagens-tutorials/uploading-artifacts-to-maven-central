Title: Deploying Artifacts to Maven Central<br>
Author: Marvin Haagen<br>
Date: 17.04.2022

# Deploying Artifacts to Maven Central
## Introduction
Sometimes you have some piece of Java code that you would like to reuse in other projects. 
Just copying the code would be a bad practise, as you would have to fix bugs, that you might find later, in all projects
that you use the code in. For this there are the Maven Artifacts that can automatically downloaded from Maven Central.
So it would be nice to deploy you piece of code to it, so that is available for download anytime
and if you have to fix the code, it only needs to be done one time and then be uploaded as a new version to
Maven Central. It is also possible to setup private Maven Repositories and publish your
Artifacts to it if your policy does not allow for publishing any kind of code, but this
manual will focus on the necessary steps for uploading to Maven Central.

## Register your Group ID
The Group ID identifies the creator of an Artifact, so you first have to register one.

### Create a Jira account for Sonatype
If you do not have a Jira Account at [issues.sonatype.org](issues.sonatype.org), then you first
have to [create one](https://issues.sonatype.org/secure/Signup!default.jspa). When the account
is created, you will receive an email at your email address.

### Create a Jira ticket for Group ID registration
Sign into your Sonatype Jira Account at [issues.sonatype.org](issues.sonatype.org) and
[create a new ticket](https://issues.sonatype.org/secure/CreateIssue.jspa?pid=10134&issuetype=21). In the ticket enter
the Group ID you wish to register. The Group ID is the reverse notification of a domain you own. For registering personal
Group IDs see the Sonatype description on [how to choose your coordinates](https://central.sonatype.org/publish/requirements/coordinates/).
The registration can take about 2 work days. The process is finished when you a receive an email ackknowledging that the created
ticket has been resolved.

>The Group ID you have registered can be reused for other projects. Also you can use Group IDs that derive from the registered
one without needing additional registration. For example if you have successfully registered com.gitlab.marvin-haagen then you
also can use the Group ID com.gitlab.marvin-haagen.project for your artifacts without going through the registration process again.

### Setup your source code
Set Group ID, Artifact ID, Version and packaging in the pom.xml of your Maven project.

```
    <groupId>io.gitlab.marvin-haagen</groupId>
    <artifactId>maven-artifacts-demo</artifactId>
    <version>1.0</version>
    <packaging>jar</packaging>
```

Specify the Java language level (source version), the Bytecode version (target version) and
the encoding of the source files in your pom.xml.

```
    <properties>
        <maven.compiler.source>8</maven.compiler.source>
        <maven.compiler.target>8</maven.compiler.target>
        <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
    </properties>
```

Add the distribution management section to your pom.xml.
```
    <distributionManagement>
        <snapshotRepository>
            <id>central</id>
            <url>https://s01.oss.sonatype.org/content/repositories/snapshots</url>
        </snapshotRepository>
        <repository>
            <id>ossrh</id>
            <url>https://s01.oss.sonatype.org/service/local/staging/deploy/maven2/</url>
        </repository>
    </distributionManagement>
```

Specify username and password in your settings.xml, which is located in your .m2 folder

```
<settings xsi:schemaLocation="http://maven.apache.org/SETTINGS/1.1.0 http://maven.apache.org/xsd/settings-1.1.0.xsd"
xmlns="http://maven.apache.org/SETTINGS/1.1.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
    <servers>
        <server>
            <id>central</id>
            <username>MAVEN_REPO_USER</username>
            <password>MAVEN_REPO_PASS</password>
        </server>
        <server>
            <id>ossrh</id>
            <username>MAVEN_REPO_USER</username>
            <password>MAVEN_REPO_PASS</password>
        </server>
    </servers>
</settings>
```

>Replace **MAVEN_REPO_USER** with your username and **MAVEN_REPO_PASS** with your password

>The .m2 folder usually is located within your users home directory. Alternatively you
>can create a settings.xml with the content shown above in any folder and specify its location
>with the -gs command line option when running maven

Add the Nexus Staging Maven Plugin to your pom.xml. 
In the deploy phase it will deploy the build artifact to Sonatype. 
Set the configuration option autoReleaseAfterClose to true to automatically close 
and release the artifact to Maven Central after the upload to Sonatype.

```
    <build>
        <plugins>
            <plugin>
                <groupId>org.sonatype.plugins</groupId>
                <artifactId>nexus-staging-maven-plugin</artifactId>
                <version>1.6.12</version>
                <extensions>true</extensions>
                <executions>
                    <execution>
                        <id>default-deploy</id>
                        <phase>deploy</phase>
                        <goals>
                            <goal>deploy</goal>
                        </goals>
                    </execution>
                </executions>
                <configuration>
                    <serverId>ossrh</serverId>
                    <nexusUrl>https://s01.oss.sonatype.org/</nexusUrl>
                    <autoReleaseAfterClose>true</autoReleaseAfterClose>
                </configuration>
            </plugin>
        </plugins>
    </build>
```

>If you wish to use another provider than Sonatype for deploying to Maven Central use the Maven Deploy Plugin instead.
>Please take a look into the [Sonatype manual](https://central.sonatype.org/publish/publish-maven/#distribution-management-and-authentication).

Sonatype has some ruleset that an artifact has to pass for successfully releasing it.
One requirement is that it has to have the source code and javadoc attached as separate JAR files.
For this, add the Maven Javadoc Plugin and Maven Source Plugin to the build plugins section of your pom.xml.
```
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-javadoc-plugin</artifactId>
                <version>3.3.2</version>
                <executions>
                    <execution>
                        <id>attach-javadocs</id>
                        <phase>package</phase>
                        <goals>
                            <goal>jar</goal>
                        </goals>
                    </execution>
                </executions>
            </plugin>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-source-plugin</artifactId>
                <version>3.2.1</version>
                <executions>
                    <execution>
                        <id>attach-sources</id>
                        <phase>package</phase>
                        <goals>
                            <goal>jar-no-fork</goal>
                        </goals>
                    </execution>
                </executions>
            </plugin>
```

You need to sign your artifacts. For this, install GnuPG and add the 
Maven GPG Plugin to the build plugins section of your pom.xml.
```
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-gpg-plugin</artifactId>
                <version>3.0.1</version>
                <executions>
                    <execution>
                        <id>sign-artifacts</id>
                        <phase>package</phase>
                        <goals>
                            <goal>sign</goal>
                        </goals>
                    </execution>
                </executions>
                <configuration>
                    <keyname>91AE7018DC31FCC0DFACFD892E1F2939432EA312</keyname>
                    <useAgent>true</useAgent>
                    <gpgArguments>
                        <arg>--verbose</arg>
                    </gpgArguments>
                </configuration>
            </plugin>
```

>If the Maven GPG Plugin fails on execution, because it does not find the 
>GPG executable you need to specify its location within the configuration section
>of the Maven GPG Plugin:<br>
>`<executable>C:\Program Files (x86)\gnupg\bin\gpg.exe</executable>`

Now create your keypair for signing your artifacts:
```
gpg --gen-key
```

And export your keys:
```
gpg --armor --export > public-key.asc
gpg --armor --export-secret-keys > secret-key.asc
```
>The asc file of the secret key is a backup that you can use to restore your key
>if you need to reimport it into your GPG keyring. So store it on a device where it does
>not get lost and ensure that only authorized persons have access to it.

>You should keep the asc file of the public key, so that you can use it when
>you need to upload it to a keyserver by using a website or when you need to 
>directly distribute your public key to other persons.

And send your public key to one of the supported keyservers:
```
gpg --keyserver keyserver.ubuntu.com --send-keys YOUR-KEY-ID
```

Currently Maven Central supports following keyservers:
- keyserver.ubuntu.com
- keys.openpgp.org
- pgp.mit.edu

Also you need to specify some meta information about the artifact.
- Project name, description, and the projects website:
```
    <name>Maven Artifacts Demo</name>
    <description>This is a demo artifact used for a tutorial on how to upload an artifact to Maven Central.</description>
    <url>https://gitlab.com/marvin-haagens-tutorials/uploading-artifacts-to-maven-central</url>
```
- The URL of the projects version control system:
```
    <scm>
        <url>https://gitlab.com/marvin-haagens-tutorials/uploading-artifacts-to-maven-central</url>
    </scm>
```
- Information about the developers:
```
    <developers>
        <developer>
            <id>MH</id>
            <name>Marvin Haagen</name>
            <email>email@something.xyz</email>
            <roles>
                <role>architect</role>
                <role>developer</role>
            </roles>
        </developer>
    </developers>
```
- The license under which the artifact is being released:
```
    <licenses>
        <license>
            <name>MIT License</name>
            <url>https://opensource.org/licenses/MIT</url>
            <distribution>repo</distribution>
        </license>
    </licenses>
```

Now you can deploy your artifact to Maven Central:
```
mvn clean deploy
```

Check if the artifact has been closed. For this, log into your
[Sonatype repository](https://s01.oss.sonatype.org/#stagingRepositories) with you credentials from [issues.sonatype.org](issues.sonatype.org).
If you see your artifact in the list, then you need to manually do the release.
1. Close your artifact. For this you need to click on the Button called "close". If it's grayed out, then the artifact is already closed.
   Now after some seconds, click the Button captioned "refresh".
2. The Button captioned "release" should be clickable now. Click it to release your artifact.
If the release failed, you should take a look in the **Activity** tab. If there are rule errors, you need to fix them first, before attempting the release again.

>Note: If everything worked fine except of the release and you get a "repository writeable" error, then you might have
>uploaded the artifact with the same version twice. To fix this, increment the version number in your pom.xml
>and then run `mvn clean deploy` again. Drop the old artifact version in the Sonatype repository by clicking the
>button captioned "Drop".